<?php
// from https://codeofaninja.com/2017/02/create-simple-rest-api-in-php.html
// contains properties and methods for "product" database queries.
class Magazine{
  
    // database connection and table name
    private $conn;
    private $table_name = "magazines";
  
    // object properties
    public $id;
    public $tipo_magazine;
    public $titolo;
    public $continente;
    public $stato;
    public $regione;
    public $citta;
    public $categoria;
    public $anno;
    public $mese;
    public $url_file;
    public $category_id;
    public $created;
    public $modified;
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // used when filling up the update product form
    function readOne(){
 
        // query to read single record
        $query = "SELECT
                m.id, m.tipo_magazine, m.titolo, m.continente, m.stato, m.regione, m.citta, m.categoria, m.anno, m.mese, m.url_file
                FROM
                    " . $this->table_name . " m
                   
                WHERE
                    m.id = ?
                LIMIT
                    0,1";
     
        // prepare query statement
        $stmt = $this->conn->prepare( $query );
     
        // bind id of product to be updated
        $stmt->bindParam(1, $this->id);
     
        // execute query
        $stmt->execute();
     
        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
     
        // set values to object properties
        $this->id = $row['id'];
        $this->tipo_magazine = $row['tipo_magazine'];
        $this->titolo = $row['titolo'];
        $this->continente = $row['continente'];
        $this->stato = $row['stato'];
        $this->regione = $row['regione'];
        $this->citta = $row['citta'];
        $this->categoria = $row['categoria'];
        $this->anno = $row['anno'];
        $this->mese = $row['mese'];
        $this->url_file = $row['url_file'];
    }
    
    // read magazines
function read(){
    // select all query
    $query = "SELECT
                m.id, m.tipo_magazine, m.titolo, m.continente, m.stato, m.regione, m.citta, m.categoria, m.anno, m.mese, m.url_file
            FROM
                " . $this->table_name . " m
            ORDER BY
                m.regione DESC";
  
    // prepare query statement
    $stmt = $this->conn->prepare($query);
  
    // execute query
    $stmt->execute();
  
    return $stmt;
    }
   // search products
function search($keywords){
 
    // select all query
    $query = "SELECT
                 m.id, m.tipo_magazine, m.titolo, m.continente, m.stato, m.regione, m.citta, m.categoria, m.anno, m.mese, m.url_file
            FROM
                " . $this->table_name . " m
                
            WHERE
                m.titolo LIKE ? OR m.stato LIKE ? OR m.regione LIKE ? OR m.citta LIKE ?
            ORDER BY
                m.citta DESC";
 
    // prepare query statement
    $stmt = $this->conn->prepare($query);
 
    // sanitize
    $keywords=htmlspecialchars(strip_tags($keywords));
    $keywords = "%{$keywords}%";
 
    // bind
    $stmt->bindParam(1, $keywords);
    $stmt->bindParam(2, $keywords);
    $stmt->bindParam(3, $keywords);
    $stmt->bindParam(4, $keywords);
 
    // execute query
    $stmt->execute();
 
    return $stmt;
} 
// read magazines with pagination
public function readPaging($from_record_num, $records_per_page){
 
    // select query
    $query = "SELECT
                m.id, m.tipo_magazine, m.titolo, m.continente, m.stato, m.regione, m.citta, m.categoria, m.anno, m.mese, m.url_file
            FROM
                " . $this->table_name . " m
                
            ORDER BY m.citta DESC
            LIMIT ?, ?";
 
    // prepare query statement
    $stmt = $this->conn->prepare( $query );
 
    // bind variable values
    $stmt->bindParam(1, $from_record_num, PDO::PARAM_INT);
    $stmt->bindParam(2, $records_per_page, PDO::PARAM_INT);
 
    // execute query
    $stmt->execute();
 
    // return values from database
    return $stmt;
}
// used for paging products
public function count(){
    $query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name . "";
 
    $stmt = $this->conn->prepare( $query );
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
 
    return $row['total_rows'];
}
}
?>