<?php
//a file that will output JSON data based on "magazines" database records
// from https://codeofaninja.com/2017/02/create-simple-rest-api-in-php.html

// include database and object files
include_once '../config/database.php';
include_once '../objects/magazine.php';
  
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$magazine = new Magazine($db);
  
// read magazines will be here
// query magazines
$stmt = $magazine->read();
$num = $stmt->rowCount();
  
// check if more than 0 record found
if($num>0){
  
    // magazines array
    $magazines_arr=array();
    $magazines_arr["records"]=array();
  
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
  
        $magazine_item=array(
            "id" => $id,
            "tipo_magazine" => $tipo_magazine,
            "titolo" => html_entity_decode($titolo),
            "continente" => $continente,
            "stato" => $stato,
            "regione" => $regione,
            "citta" => $citta,
            "categoria" => $categoria,
            "anno" => $anno,
            "mese" => $mese,
            "url_file" => $url_file
        );
  
        array_push($magazines_arr["records"], $magazine_item);
    }
  
    // set response code - 200 OK
    http_response_code(200);
  
    // show magazines data in json format
    echo json_encode($magazines_arr);
}
  
// no magazines found will be here
else{
 
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no products found
    echo json_encode(
        array("message" => "No magazines found.")
    );
}

?>