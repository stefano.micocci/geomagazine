<?php
//a file that will output JSON data based on "magazines" database records
// from https://codeofaninja.com/2017/02/create-simple-rest-api-in-php.html
//
// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

// include database and object files
include_once '../config/database.php';
include_once '../objects/magazine.php';
  
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();
  
// initialize object
$magazine = new Magazine($db);

// set ID property of record to read
$magazine->id = isset($_GET['id']) ? $_GET['id'] : die();
 
// read the details of product to be edited
$magazine->readOne();
  
  
// check if more than 0 record found
if($magazine->titolo!=null){
    // create array
        $magazine_arr=array(
            "id" => $magazine->id,
            "tipo_magazine" => $magazine->tipo_magazine,
            "titolo" => $magazine->titolo,
            "continente" => $magazine->continente,
            "stato" => $magazine->stato,
            "regione" => $magazine->regione,
            "citta" => $magazine->citta,
            "categoria" => $magazine->categoria,
            "anno" => $magazine->anno,
            "mese" => $magazine->mese,
            "url_file" => $magazine->url_file
        );
  
  
    // set response code - 200 OK
    http_response_code(200);
  
    // show magazines data in json format
    echo json_encode($magazine_arr);
}
  
// no magazines found will be here
else{
 
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no products found
    echo json_encode(
        array("message" => "No magazines found.")
    );
}

?>